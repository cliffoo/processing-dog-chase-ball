import math
import random

def setup():
    global bgImg
    global dogX, dogY, dogW, dogH, dogImg
    global ballX, ballY, ballW, ballH, ballImg
    global ballIncrementX, ballIncrementY, dogIncrementX, dogIncrementY, defaultBallIncrementX, defaultBallIncrementY, ballVelocityMagnitude, ballRandomness
    global dogVelocityIncrement, dogMaxVelocityMagnitude, dogVelocityMagnitude, bounceCounter, dogVelocityIncrementThreshold, dogVelocityIncrementCounter, time

    size(700, 700)
    bgImg = loadImage("grass.jpg")
    dogX = 100
    dogY = 100
    dogW, dogH = 60, 60
    dogImg = loadImage("growlithe.png")
    ballX = 600
    ballY = 100
    ballW, ballH = 40, 40
    ballImg = loadImage("ball.png")
    ballIncrementX = 6
    ballIncrementY = 6
    ballVelocityMagnitude = math.sqrt(ballIncrementX ** 2 + ballIncrementY ** 2)
    defaultBallIncrementX, defaultBallIncrementY = ballIncrementX, ballIncrementY
    dogIncrementX, dogIncrementY = 0, 0
    ballRandomness = 0.8
    dogVelocityMagnitude = ballVelocityMagnitude * 0.1
    dogVelocityIncrement = ballVelocityMagnitude * 0.1
    dogMaxVelocityMagnitude = ballVelocityMagnitude * 0.5
    bounceCounter = 0
    dogVelocityIncrementThreshold = 5
    dogVelocityIncrementCounter = 0
    time = millis()

def draw():
    global time
    background(bgImg)
    if dogX <= ballX:
        dogImg = loadImage("growlithe.png")
    else:
        dogImg = loadImage("growlithe_flipped.png")
    collided = detectCollision()
    if collided:
        timePassed = millis() - time
        if timePassed >= 400:
            image(dogImg, dogX, dogY, dogW, dogH)
            image(ballImg, ballX, ballY, ballW, ballH)
            if timePassed >= 800:
                time = millis()
    else:
        move()
        adjustBallIncrements()
        adjustDogIncrements()
        image(dogImg, dogX, dogY, dogW, dogH)
        image(ballImg, ballX, ballY, ballW, ballH)

def move():
    global ballX, ballY
    global dogX, dogY
    ballX += ballIncrementX
    ballY += ballIncrementY
    dogX += dogIncrementX
    dogY += dogIncrementY

def adjustBallIncrements():
    global ballX, ballY
    global ballIncrementX, ballIncrementY, defaultBallIncrementX, defaultBallIncrementY, ballW, ballH
    global bounceCounter
    # Check left and right bounds collisions
    if ballX >= (700 - ballW) or ballX <= 0:
        ballIncrementX, ballIncrementY = getTweakedVectorComponents(ballIncrementX, ballIncrementY, "x", defaultBallIncrementX, defaultBallIncrementY)
        bounceCounter += 1
    # Check top and bottom bounds collisions
    if ballY >= (700 - ballH) or ballY <= 0:
        ballIncrementX, ballIncrementY = getTweakedVectorComponents(ballIncrementX, ballIncrementY, "y", defaultBallIncrementX, defaultBallIncrementY)
        bounceCounter += 1

def getTweakedVectorComponents(currentXIncrement, currentYIncrement, axis, defaultXIncrement, defaultYIncrement):
    xIncrementSign = getSign(currentXIncrement)
    yIncrementSign = getSign(currentYIncrement)
    change = random.uniform(-ballRandomness, ballRandomness)
    # change y magnitude on alternate bounces here (or if one magnitude is a lot larger than the other)
    newXMagnitude = abs(currentXIncrement) * (1 + change)
    while newXMagnitude >= ballVelocityMagnitude:
        newXMagnitude *= 1 - change * 0.2
    newYMagnitude = math.sqrt(ballVelocityMagnitude**2 - newXMagnitude**2)
    if (newXMagnitude*0.2 > newYMagnitude) or (newYMagnitude*0.2 > newXMagnitude):
        newXMagnitude, newYMagnitude = defaultXIncrement, defaultYIncrement
    newXIncrement = newXMagnitude
    newYIncrement = newYMagnitude
    if xIncrementSign == -1:
        newXIncrement *= -1
    if yIncrementSign == -1:
        newYIncrement *= -1
    if axis == "x":
        return newXIncrement * -1, newYIncrement
    else:
        return newXIncrement, newYIncrement * -1

def adjustDogIncrements():
    global ballX, ballY, ballIncrementY, ballIncrementX
    global dogX, dogY, dogIncrementX, dogIncrementY
    global bounceCounter, dogVelocityMagnitude, dogVelocityIncrement, dogMaxVelocityMagnitude, dogVelocityIncrementThreshold, dogVelocityIncrementCounter
    
    if bounceCounter // dogVelocityIncrementThreshold != dogVelocityIncrementCounter and dogVelocityMagnitude < dogMaxVelocityMagnitude:
        dogVelocityMagnitude += dogVelocityIncrement
        dogVelocityIncrementCounter += 1

    xDisplacement = ballX - dogX
    yDisplacement = ballY - dogY
    displacementVectorMagnitude = math.sqrt(yDisplacement**2 + xDisplacement**2)
    ratio = dogVelocityMagnitude / displacementVectorMagnitude
    dogIncrementX = ratio * xDisplacement
    dogIncrementY = ratio * yDisplacement

def getSign(num):
    if num >= 0:
        return 1
    else:
        return -1

def detectCollision():
    if ballX + ballW >= dogX and ballX <= dogX + dogW and ballY + ballH >= dogY and ballY <= dogY + dogH:
        return True
    else:
        return False
